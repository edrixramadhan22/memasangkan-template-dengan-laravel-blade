@extends('master')

    @section('judul')
        SanberBook
    @endsection

    @section('tab')
        Apa sih SanberBook itu ?
    @endsection

    @section('content')
        <h5>Social Media Developer Santai Berkualitas</h5>
            <p>Belajar dan Berbagi agar hidup ini semakin santai dan berkualitas</p>
        <h5>Benefit Join di SanberBook</h5>
            <ul>
                <li>Mendapatkan motivasi dari sesama Developer</li>
                <li>Sharing pengetahuan dari para Mastah Sanber</li>
                <li>Dibuat oleh calon Web Developer terbaik</li>
            </ul>
        <h5>Cara Bergabung ke SanberBook</h5>
            <ol>
                <li>Mengunjungi website ini</li>
                <li>Mendaftar di Form <a href="/form">Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
    @endsection