@extends('master')
    
    @section('judul')
        SanberBook
    @endsection

    @section('tab')
        Buat Account Baru!
    @endsection
        
    @section('content')
        <h4>Sign Up Form</h4>
        <form action="/loged" method="POST">
            @csrf
            <label for="fname">First name:</label><br>
            <input type="text" id="fname" name="fname"><br>
            <label for="mname">Middle name: (<i>optional</i>)</label><br>
            <input type="text" id="mname" name="mname"><br>
            <label for="lname">Last name:</label><br>
            <input type="text" id="lname" name="lname"><br><br>
            <label for="gender">Gender:</label><br>
            <input type="radio" id="gender" name="gender">Male<br>
            <input type="radio" id="gender" name="gender">Female<br>
            <input type="radio" id="gender" name="gender">Other<br><br>
            <label for="kwn">Nationality:</label><br>
            <select name="kwn"><br>
                <option value="1">Indonesia</option>
                <option value="2">Malaysia</option>
                <option value="3">Singapore</option>
            </select><br><br>
            <label for="bhs">Language Spoken:</label><br>
            <input type="checkbox" id="bhs" name="bhs">Indonesia<br>
            <input type="checkbox" id="bhs" name="bhs">English<br>
            <input type="checkbox" id="bhs" name="bhs">Other<br><br>
            <label for="bio">Bio:</label><br>
            <textarea name="bio" cols="60" rows="10"></textarea><br><br>
            
            <br><input type="submit" value="Sign Up"><br><br>
          </form> 
    @endsection

        
