<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','WelcomeController@home');

Route::get('/form','RegisterController@form');

Route::post('/loged','RegisterController@submit');

Route::get('/master', function(){
    return view('master');
});

Route::get('/data-table', function(){
    return view('tables.datatable');
});

Route::get('/table', function(){
    return view('tables.table');
});